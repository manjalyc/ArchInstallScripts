#!/bin/bash

username=`cat vars/username`
disk=`cat vars/disk`
mountpoint=`cat vars/mountpoint`

read -n1 -r -p "RUN AS ROOT: Add User $username (wheel) to $disk mounted at $mountpoint"
echo "> mount $disk $mountpoint"
mount $disk $mountpoint
echo "> arch-chroot $mountpoint useradd -m -G wheel -s /bin/bash $username"
arch-chroot $mountpoint useradd -m -G wheel -s /bin/bash $username
echo "> arch-chroot $mountpoint passwd $username"
arch-chroot $mountpoint passwd $username
echo "> arch-chroot $mountpoint nano /etc/sudoers"
echo "%wheel ALL=(ALL) ALL" >> $mountpoint/etc/sudoers
