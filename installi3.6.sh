#!/bin/bash

username=`cat vars/username`
disk=`cat vars/disk`
mountpoint=`cat vars/mountpoint`

packages="i3 dunst dmenu nautilus xorg-xbacklight alsa-utils gnome-system-monitor xfce4-screenshooter xfce4-clipman-plugin lxappearance kmenuedit systemsettings plasma-desktop scrot dunst xclip"
#lxappearance for editing gtk themes
#kmenuedit systemsettings plasma-desktop for editing qt themes

read -n1 -r -p "RUN AS ROOT: Install i3 to $disk mounted at $mountpoint"
echo "> pacstrap $mountpoint $packages"
pacstrap $mountpoint $packages
echo "export XDG_CURRENT_DESKTOP=kde" > $mountpoint/home/$username/.xinitrc
echo "exec i3" >> $mountpoint/home/$username/.xinitrc
echo "feh --bg-fill ~/Scripts/Dotfiles/i3/Background.png" >> $mountpoint/home/$username/.xinitrc
