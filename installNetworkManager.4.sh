#!/bin/bash

disk=`cat vars/disk`
mountpoint=`cat vars/mountpoint`

read -n1 -r -p "RUN AS ROOT: Install NetworkManager to $disk mounted at $mountpoint"
echo "> pacstrap $mountpoint gnome-keyring networkmanager network-manager-applet"
pacstrap $mountpoint gnome-keyring networkmanager network-manager-applet
echo "> systemctl disable dhcpcd"
arch-chroot $mountpoint systemctl disable dhcpcd
echo "> systemctl disable dhcpcd@ens33"
arch-chroot $mountpoint systemctl disable dhcpcd@ens33
echo "> systemctl enable NetworkManager"
arch-chroot $mountpoint systemctl enable NetworkManager

echo "Nameserver Configuration -- Google"
echo "#\!/bin/bash" > $mountpoint/etc/NetworkManager/dispatcher.d/dnsover.sh
echo "cp -f /etc/resolv.conf.override /etc/resolv.conf" >> $mountpoint/etc/NetworkManager/dispatcher.d/dnsover.sh
chmod +x $mountpoint/etc/NetworkManager/dispatcher.d/dnsover.sh

echo "#Cloudfare nameservers" > $mountpoint/etc/resolv.conf.override
echo "nameserver 1.1.1.1" >> $mountpoint/etc/resolv.conf.override
echo "nameserver 1.0.0.1" >> $mountpoint/etc/resolv.conf.override
echo "#nameserver 2606:4700:4700::1111" >> $mountpoint/etc/resolv.conf.override
echo "#nameserver 2606:4700:4700::1001" >> $mountpoint/etc/resolv.conf.override

echo "# Google IPv4 nameservers" > $mountpoint/etc/resolv.conf.override
echo "#nameserver 8.8.8.8" >> $mountpoint/etc/resolv.conf.override
echo "#nameserver 8.8.4.4" >> $mountpoint/etc/resolv.conf.override
echo "# Google IPv6 nameservers" >> $mountpoint/etc/resolv.conf.override
echo "#nameserver 2001:4860:4860::8888" >> $mountpoint/etc/resolv.conf.override
echo "#nameserver 2001:4860:4860::8844" >> $mountpoint/etc/resolv.conf.override
