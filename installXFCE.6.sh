#!/bin/bash

username=`cat vars/username`
disk=`cat vars/disk`
mountpoint=`cat vars/mountpoint`

packages="xfce4 nautilus xorg-xbacklight alsa-utils gnome-system-monitor xfce4-whiskermenu-plugin xfce4-clipman-plugin xfce4-datetime-plugin xfce4-notifyd xfce4-pulseaudio-plugin xfce4-screenshooter xfce4-mount-plugin xfce4-sensors-plugin lxappearance kmenuedit systemsettings plasma-desktop scrot dunst xclip"
#lxappearance for editing gtk themes
#kmenuedit systemsettings plasma-desktop for editing qt themes

read -n1 -r -p "RUN AS ROOT: Install i3 to $disk mounted at $mountpoint"
echo "> pacstrap $mountpoint $packages"
pacstrap $mountpoint $packages
#echo "export XDG_CURRENT_DESKTOP=kde" > $mountpoint/home/$username/.xinitrc
echo "exec startxfce4" >> $mountpoint/home/$username/.xinitrc
