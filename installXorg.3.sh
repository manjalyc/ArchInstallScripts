#!/bin/bash


username=`cat vars/username`
disk=`cat vars/disk`
mountpoint=`cat vars/mountpoint`

packages="xorg-server xterm xorg-docs xorg-fonts-100dpi xorg-fonts-75dpi xorg-server-devel xorg-server-xdmx xorg-server-xephyr xorg-server-xnest xorg-server-xvfb xorg-smproxy xorg-twm xorg-x11perf xorg-xclock xorg-xcursorgen xorg-xinit xorg-xkbevd xorg-xkbutils xorg-xkill xorg-xpr xorg-xwd xorg-xwud xf86-input-libinput xf86-input-synaptics xf86-input-vmmouse xf86-input-void xf86-video-intel"
read -n1 -r -p "Install Xorg & Touchpad Support for intel to $disk mounted at $mountpoint?"
echo "> mount $disk $mountpoint"
mount $disk $mountpoint
echo "> pacstrap $mountpoint $packages"
pacstrap $mountpoint $packages

