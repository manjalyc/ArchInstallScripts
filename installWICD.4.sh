#!/bin/bash

disk=`cat vars/disk`
mountpoint=`cat vars/mountpoint`
username=`cat vars/username`

read -n1 -r -p "RUN AS ROOT: Install WICD to $disk mounted at $mountpoint"
echo "> pacstrap $mountpoint wicd wicd-gtk gnome-keyring"
pacstrap $mountpoint wicd wicd-gtk gnome-keyring
echo "> systemctl disable dhcpcd"
arch-chroot $mountpoint systemctl disable dhcpcd
echo "> systemctl disable dhcpcd@ens33"
arch-chroot $mountpoint systemctl disable dhcpcd@ens33
echo "> systemctl enable wicd"
arch-chroot $mountpoint systemctl enable wicd
arch-chroot $mountpoint gpasswd -a $username users
