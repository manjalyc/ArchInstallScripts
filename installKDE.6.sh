#!/bin/bash

username=`cat vars/username`
disk=`cat vars/disk`
mountpoint=`cat vars/mountpoint`

packages="plasma-desktop dolphin breeze-kde4 breeze-gtk konsole plasma-nm plasma-pa print-manager"

read -n1 -r -p "RUN AS ROOT: Install KDE to $disk mounted at $mountpoint"
echo "> pacstrap $mountpoint $packages"
pacstrap $mountpoint $packages
echo "exec startkde" > $mountpoint/home/$username/.xinitrc
