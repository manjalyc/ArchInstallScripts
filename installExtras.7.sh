#!/bin/bash

username=`cat vars/username`
disk=`cat vars/disk`
mountpoint=`cat vars/mountpoint`

packages="adobe-source-sans-pro-fonts adobe-source-serif-pro-fonts awesome-terminal-fonts bluez-utils breeze-gtk breeze-kde4 calibre chromium compton dolphin eclipse-common eclipse-java efibootmgr efivar feh ffmpegthumbnailer gconf gnome-calculator gnome-terminal gnumeric gparted gummi gvfs-afc gvfs-goa gvfs-google gvfs-gphoto2 gvfs-mtp gvfs-nfs gvfs-smb hwinfo konsole mate-terminal mplayer pepper-flash playerctl projectm projectm-pulseaudio projectm-qt python-pip refind-efi skanlite steam steam-native-runtime strace texlive-bin texlive-core texlive-fontsextra texlive-formatsextra texlive-latexextra ttf-dejavu ttf-liberation ttf-ubuntu-font-family wavpack wine wine-mono winetricks wxgtk-common wxgtk2 xcompmgr xsel emby-server p7zip"

read -n1 -r -p "RUN AS ROOT: Installing to $disk mounted at $mountpoint"

echo "[multilib]" >> $mountpoint/etc/pacman.conf
echo "Include = /etc/pacman.d/mirrorlist" >> $mountpoint/etc/pacman.conf

echo "> arch-chroot $mountpoint pacman -S --needed gcc-libs-multilib"
arch-chroot $mountpoint pacman -S --needed gcc-libs-multilib

echo "> pacstrap $mountpoint $packages"
pacstrap $mountpoint $packages


echo "$username ALL=(ALL) NOPASSWD: /usr/bin/systemctl start emby-server" >> $mountpoint/etc/sudoers
echo "$username ALL=(ALL) NOPASSWD: /usr/bin/systemctl stop emby-server" >> $mountpoint/etc/sudoers
