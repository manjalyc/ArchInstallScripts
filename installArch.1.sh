#!/bin/bash

disk=`cat vars/disk`
mountpoint=`cat vars/mountpoint`
packages="base base-devel arch-install-scripts intel-ucode exfat-utils dosfstools ntfsprogs"
tzone="America/New_York"
locale="en_US.UTF-8 UTF-8"
localeconf="LANG=en_US.UTF-8"
lang="en_US.UTF-8"
hostname=`cat vars/hostname`
hosts="127.0.1.1 $hostname.localdomain $hostname"

#Formatting & Mounting Disks
read -n1 -r -p "MUST BE RUN AS ROOT: Press any key to continue..."
lsblk
echo "Formatting (ext4) & Mounting $disk to $mountpoint"
read -n1 -r -p "Press any key to continue..."
umount $disk
echo "> mkfs.ext4 $disk"
mkfs.ext4 $disk
echo "> mkdir $mountpoint"
mkdir $mountpoint
echo "> mount $disk $mountpoint && echo "Mounted $disk to $mountpoint"
lsblk"
mount $disk $mountpoint && echo "Mounted $disk to $mountpoint"
lsblk

#Install the base packages
read -n1 -r -p "Installing $packages, Press any key to continue..."
echo "> pacstrap $mountpoint $packages"
pacstrap $mountpoint $packages
echo "> genfstab -U $mountpoint >> $mountpoint/etc/fstab"
genfstab -U $mountpoint >> $mountpoint/etc/fstab

#Set Time Zone
read -n1 -r -p "Set time zone to $tzone, Press any key to continue..."
echo "> arch-chroot $mountpoint ln -sf /usr/share/zoneinfo/$tzone /etc/localtime"
rm $mountpoint/etc/localtime
arch-chroot $mountpoint ln -sf /usr/share/zoneinfo/$tzone /etc/localtime
echo "> arch-chroot $mountpoint hwclock --systohc"
arch-chroot $mountpoint hwclock --systohc

#Set and Generate Locales
read -n1 -r -p "Set locale to $locale & lang to $lang, Press any key to continue..."
echo "> echo $locale >> $mountpoint/etc/locale.gen"
echo $locale >> $mountpoint/etc/locale.gen
echo "> arch-chroot $mountpoint locale-gen"
arch-chroot $mountpoint locale-gen
echo "> echo $localeconf > $mountpoint/etc/locale.conf"
echo $localeconf > $mountpoint/etc/locale.conf

#Set hostname
read -n1 -r -p "Setting the hostname to $hostname"
echo "> echo $hostname > $mountpoint/etc/hostname"
echo $hostname > $mountpoint/etc/hostname
echo "> echo $hosts >> $mountpoint/etc/hosts"
echo $hosts >> $mountpoint/etc/hosts

#NetworkManager
read -n1 -r -p "Setting up console-based wifi management, Press any key to continue..."
echo "> pacstrap $mountpoint iw wpa_supplicant dialog"
pacstrap $mountpoint iw wpa_supplicant dialog

#Root Password
read -n1 -r -p "Set up the root password, you will be prompted to enter a password, Press any key to continue..."
echo "> arch-chroot $mountpoint passwd"
arch-chroot $mountpoint passwd

#Wifi Management
echo "options rtl8723be fwlps=0 ant_sel=1" > $mountpoint/etc/modprobe.d/rtl8723be.conf

#Sysrq
sudo echo "kernel.sysrq = 1" >> $mountpoint/etc/sysctl.d/99-sysctl.conf
