#!/bin/bash

username=`cat vars/username`
disk=`cat vars/disk`
mountpoint=`cat vars/mountpoint`

packages="pacman-contrib gedit cmus deluge eog evince firefox git hyphen-en ifuse libreoffice-fresh pamixer pavucontrol pulseaudio-bluetooth pulseaudio-equalizer pulseaudio-alsa rsync wget xterm tor torsocks vlc youtube-dl flashplugin gimp gnome-screenshot xclip aria2 zenity languagetool ncdu xorg-xbacklight scrot qt5ct"

read -n1 -r -p "RUN AS ROOT: Installing to $disk mounted at $mountpoint"
echo "> pacstrap $mountpoint $packages"
pacstrap $mountpoint $packages
echo "$username ALL=(ALL) NOPASSWD: /usr/bin/systemctl start tor" >> $mountpoint/etc/sudoers
echo "$username ALL=(ALL) NOPASSWD: /usr/bin/systemctl stop tor" >> $mountpoint/etc/sudoers

echo "$username ALL=(ALL) NOPASSWD: /usr/bin/pacman -Syy" >> $mountpoint/etc/sudoers

echo "export QT_QPA_PLATFORMTHEME=\"qt5ct\"" >> $mountpoint/home/$username/.profile
