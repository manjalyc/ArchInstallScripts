#!/bin/bash

disk=`cat vars/disk`
mountpoint=`cat vars/mountpoint`

read -n1 -r -p "Install cups to $disk mounted at $mountpoint?"
echo "> mount $disk $mountpoint"
mount $disk $mountpoint
echo "> pacstrap $mountpoint cups cups-pdf hplip gtk3-print-backends"
pacstrap $mountpoint cups cups-pdf hplip
echo "> arch-chroot $mountpoint systemctl enable org.cups.cupsd.service" 
arch-chroot $mountpoint systemctl enable org.cups.cupsd.service

