#!/bin/bash

echo "Must Be Run on Local Install"

yaourtPackages="archey-git cli-visualizer etcher google-chrome i3lock-custom neofetch-git numix-circle-icon-theme-git numix-icon-theme-git osx-arc-shadow osx-arc-white otf-fira-code package-query polybar-git spotify ttf-fira-code ttf-ms-fonts xcursor-dmz xcursor-numix xcursor-oxygen yaourt"

yaourt -S --noconfirm --needed $yaourtPackages

#because this download takes forever
yaourt -S --noconfirm --needed libreoffice-extension-languagetool
